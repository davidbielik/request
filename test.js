"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var request_1 = (0, tslib_1.__importDefault)(require("./request"));
var chai_1 = require("chai");
describe("@bielik/request", function () {
    it('should work with default imports', function () {
        (0, chai_1.expect)(request_1.default).to.be.a('function');
    });
    it('should work with a module import', function () {
        (0, chai_1.expect)(require(".")).to.be.a('function');
    });
});
