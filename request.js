"use strict";
var tslib_1 = require("tslib");
//@ts-check
var request_1 = (0, tslib_1.__importDefault)(require("request"));
module.exports = function _request(config, rawResponse) {
    if (rawResponse === void 0) { rawResponse = false; }
    return new Promise(function (resolve, reject) {
        if (config.hasOwnProperty('debug') || process.env.DEBUG_REQUESTS === '1') {
            console.log('[@bielik/request]', config);
        }
        (0, request_1.default)(config, function (error, response, body) {
            if (error) {
                return reject(error);
            }
            if (response && response.statusCode >= 300) {
                return reject(rawResponse ? response : body);
            }
            return resolve(rawResponse ? response : body);
        });
    });
};
