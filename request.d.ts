import request from "request";
declare const _default: <T>(config: request.OptionsWithUri | (request.UrlOptions & request.CoreOptions), rawResponse?: boolean) => Promise<T>;
export = _default;
//# sourceMappingURL=request.d.ts.map