import request from "./request";
import { expect } from "chai";
describe("@bielik/request", function () {
    it('should work with default imports', function () {
        expect(request).to.be.a('function');
    });
    it('should work with a module import', async function () {
        expect(require(".")).to.be.a('function');

        // var a = await request<string>({uri: 'http://'})
        // var b = await request<string>({uri: 'http://'}, true)
    });
});
