import request, {Response} from "request";


function _request<T>(config: request.OptionsWithUri | (request.UrlOptions & request.CoreOptions)): Promise<T>;
function _request<T>(config: request.OptionsWithUri | (request.UrlOptions & request.CoreOptions), rawResponse: true): Promise<Response & {body: T}>;

function _request<T>(config: request.OptionsWithUri | (request.UrlOptions & request.CoreOptions), rawResponse = false): Promise<Response & {body: T} | T> {
    return new Promise((resolve, reject) => {
        if (config.hasOwnProperty('debug') || process.env.DEBUG_REQUESTS === '1') {
            console.log('[@bielik/request]', config);
        }
        request(config, (error, response, body) => {
            if (error) {
                return reject(error);
            }
            if (response && response.statusCode >= 300) {
                return reject(rawResponse ? response : body);
            }
            return resolve(rawResponse ? response : body);
        });
    });
};

export default _request;
